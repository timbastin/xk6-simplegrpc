// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package pb

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// StatServiceClient is the client API for StatService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type StatServiceClient interface {
	SaveStat(ctx context.Context, in *Stat, opts ...grpc.CallOption) (*SuccessReply, error)
}

type statServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewStatServiceClient(cc grpc.ClientConnInterface) StatServiceClient {
	return &statServiceClient{cc}
}

func (c *statServiceClient) SaveStat(ctx context.Context, in *Stat, opts ...grpc.CallOption) (*SuccessReply, error) {
	out := new(SuccessReply)
	err := c.cc.Invoke(ctx, "/statservice.StatService/saveStat", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// StatServiceServer is the server API for StatService service.
// All implementations must embed UnimplementedStatServiceServer
// for forward compatibility
type StatServiceServer interface {
	SaveStat(context.Context, *Stat) (*SuccessReply, error)
	mustEmbedUnimplementedStatServiceServer()
}

// UnimplementedStatServiceServer must be embedded to have forward compatible implementations.
type UnimplementedStatServiceServer struct {
}

func (UnimplementedStatServiceServer) SaveStat(context.Context, *Stat) (*SuccessReply, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SaveStat not implemented")
}
func (UnimplementedStatServiceServer) mustEmbedUnimplementedStatServiceServer() {}

// UnsafeStatServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to StatServiceServer will
// result in compilation errors.
type UnsafeStatServiceServer interface {
	mustEmbedUnimplementedStatServiceServer()
}

func RegisterStatServiceServer(s grpc.ServiceRegistrar, srv StatServiceServer) {
	s.RegisterService(&StatService_ServiceDesc, srv)
}

func _StatService_SaveStat_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Stat)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(StatServiceServer).SaveStat(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/statservice.StatService/saveStat",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(StatServiceServer).SaveStat(ctx, req.(*Stat))
	}
	return interceptor(ctx, in, info, handler)
}

// StatService_ServiceDesc is the grpc.ServiceDesc for StatService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var StatService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "statservice.StatService",
	HandlerType: (*StatServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "saveStat",
			Handler:    _StatService_SaveStat_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "statservice.proto",
}
