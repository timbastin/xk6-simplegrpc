package simplegrpc

import (
	"context"
	"encoding/json"

	"gitlab.com/timbastin/xk6-simplegrpc/pb"
	"go.k6.io/k6/js/modules"
	"go.k6.io/k6/lib"
	"go.k6.io/k6/lib/metrics"
	"go.k6.io/k6/stats"

	"google.golang.org/grpc"
	grpcstats "google.golang.org/grpc/stats"
)

type OrderService struct {
	client pb.OrderServiceClient
}

func (orderService *OrderService) ConnectToOrderService(address string) {
	conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		panic(err)
	}
	orderService.client = pb.NewOrderServiceClient(conn)
}

func (orderService *OrderService) SubmitOrder(order string) {

	var request pb.SubmitOrderRequest
	err := json.Unmarshal([]byte(order), &request)
	if err != nil {
		panic(err)
	}
	orderService.client.SubmitOrder(context.Background(), &request)
}

// TagConn implements the stats.Handler interface
func (*OrderService) TagConn(ctx context.Context, _ *grpcstats.ConnTagInfo) context.Context {
	// noop
	return ctx
}

// HandleConn implements the stats.Handler interface
func (*OrderService) HandleConn(context.Context, grpcstats.ConnStats) {
	// noop
}

// TagRPC implements the stats.Handler interface
func (*OrderService) TagRPC(ctx context.Context, _ *grpcstats.RPCTagInfo) context.Context {
	// noop
	return ctx
}

// HandleRPC implements the stats.Handler interface
func (orderService *OrderService) HandleRPC(ctx context.Context, stat grpcstats.RPCStats) {
	state := lib.GetState(ctx)
	switch s := stat.(type) {
	case *grpcstats.OutHeader:
	case *grpcstats.End:
		stats.PushIfNotDone(ctx, state.Samples, stats.ConnectedSamples{
			Samples: []stats.Sample{
				{
					Metric: metrics.GRPCReqDuration,
					Tags:   &stats.SampleTags{},
					Value:  stats.D(s.EndTime.Sub(s.BeginTime)),
					Time:   s.EndTime,
				},
			},
		})
	}
}

func init() {
	orderService := OrderService{}
	modules.Register("k6/x/simple-grpc", &orderService)
}
